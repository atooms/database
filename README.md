Atooms database
===============

[![pypi](https://img.shields.io/pypi/v/atooms-database.svg)](https://pypi.python.org/pypi/atooms-database/)
[![version](https://img.shields.io/pypi/pyversions/atooms-database.svg)](https://pypi.python.org/pypi/atooms-database/)
[![license](https://img.shields.io/pypi/l/atooms-pp.svg)](https://en.wikipedia.org/wiki/GNU_General_Public_License)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fatooms%2Fdatabase/HEAD?labpath=docs%2Findex.ipynb)
[![pipeline](https://framagit.org/atooms/database/badges/master/pipeline.svg)](https://framagit.org/atooms/database/badges/master/pipeline.svg)
[![coverage report](https://framagit.org/atooms/database/badges/master/coverage.svg?job=test:f90)](https://framagit.org/atooms/database/-/commits/master)

A database of interaction models and trajectory samples for classical molecular dynamics and Monte Carlo simulations.

Quick start
-----------

List the available models
```python
from atooms import database
for model in database.models:
    print(model["name"])
```
```
Gaussian core
Bernu-Hiwatari-Hansen-Pastore
Kob-Andersen
Lennard-Jones
Dellavalle-Gazzillo-Frattini-Pastore
Roux-Barrat-Hansen
Coslovich-Pastore
Grigera-Cavagna-Giardina-Parisi
harmonic spheres
Wahnstrom
```

Print all the available samples
```python
for sample in database.samples:
    print(sample["path"])
```
```
coslovich_pastore/0_488db481cdac35e599922a26129c3e35.xyz
kob_andersen/0_8f4a9fe755e5c1966c10b50c9a53e6bf.xyz
grigera_cavagna_giardina_parisi/0_0ac97fa8c69c320e48bd1fca80855e8a.xyz
lennard_jones/0_5cc3b80bc415fa5c262e83410ca65779.xyz
lennard_jones/0_13ce47602b259f7802e89e23ffd57f19.xyz
bernu_hiwatari_hansen_pastore/0_f61d7e58b9656cf9640f6e5754441930.xyz
```

Get a local copy of a Lennard-Jones fluid sample
```python
local_file = database.samples.copy("lennard_jones/0_5cc3b80bc415fa5c262e83410ca65779.xyz")
```

The `local_file` can then be used to start a simulation or further analysis using `atooms` packages.

Documentation
-------------
Check out the [documentation](https://atooms.frama.io/docs/database.html) for full details.

Installation
------------
Clone the code repository and install from source
```
git clone https://framagit.org/atooms/database.git
cd database
pip install .
```

Install `atooms-database` with pip
```
pip install atooms-database
```

Contributing
------------
Contributions to the project are welcome. If you wish to contribute, check out [these guidelines](https://framagit.org/atooms/atooms/-/blob/master/CONTRIBUTING.md).

Authors
-------
Daniele Coslovich: https://www.units.it/daniele.coslovich/
