#!/usr/bin/env python

import os
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open('README.md') as f:
    readme = f.read()

setup(name='atooms-database',
      version='0.3.1',
      description='Database of interaction database for classical molecular dynamics and Monte Carlo simulations',
      long_description=readme,
      long_description_content_type="text/markdown",
      author='Daniele Coslovich',
      author_email='dcoslovich@units.it',
      url='https://framagit.org/atooms/database',
      packages=['atooms', 'atooms/database', 'atooms/database/f90', 'atooms/database/rumd'],
      license='GPLv3',
      install_requires=['atooms~=3.19', 'f2py_jit~=0.5', 'jsonschema', 'tinydb', 'tqdm'],
      scripts=[],
      package_data={'atooms': ['database/f90/*.f90', 'database/_models.json', 'database/_samples.json',  'database/storage/*', 'database/storage/*/*']},
      include_package_data=False,
      zip_safe=False,
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
      ]
)
