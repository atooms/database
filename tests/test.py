#!/usr/bin/env python

import os
import unittest
import atooms.database as db
from atooms.core.utils import mkdir, rmf, rmd
from atooms.system import System
from atooms.trajectory import Trajectory
from atooms.database import _TinyDB

root = os.path.dirname(__file__)

class Test(unittest.TestCase):

    def setUp(self):
        for rho in [1.0]:
            for n in range(2):
                outdir = f"/tmp/dataset/rho{rho}/n{n}"
                mkdir(outdir)
                system = System(N=100)
                with Trajectory(f"{outdir}/config.xyz", "w") as th:
                    for i in range(1):
                        th.write(system, step=i)

    def _compare_data(self, ref, copy):
        with open(ref) as fh:
            data = fh.read()
        with open(copy) as fh:
            data_copy = fh.read()
        self.assertEqual(data, data_copy)

    def test_grammar(self):
        db.models()
        db.model("lennard_jones")
        db.samples()
        db.sample("lennard_jones/0_5cc3b80bc415fa5c262e83410ca65779.xyz")        
        
    def test(self):
        sample = db.samples.copy(db.query.path == "storage/lennard_jones/0_5cc3b80bc415fa5c262e83410ca65779.xyz")[0]
        raw = root + '/../atooms/database/storage/lennard_jones/0_5cc3b80bc415fa5c262e83410ca65779.xyz'
        self._compare_data(raw, sample)

    def test_convert(self):
        m = {
            "reference": "W. Kob and H. C. Andersen, Phys. Rev. Lett. 73, 1376 (1994)",
            "potential": [
                {
                    "type": "lennard_jones",
                    "parameters": {"sigma": [[1.0, 0.8], [0.8, 0.88]],
                                   "epsilon": [[1.0, 1.5], [1.5, 0.5]]}
                }
            ],
            "cutoff": [
                {
                    "type": "cut_shift",
                    "parameters": {"rcut": [[2.5, 2.0], [2.0, 2.2]]}
                }
            ]
        }
        n = {'potential': [{'cutoff': {'parameters': {'1-1': {'rcut': 2.5},
                                                      '1-2': {'rcut': 2.0},
                                                      '2-2': {'rcut': 2.2}},
                                       'type': 'cut_shift'},
                            'parameters': {'1-1': {'epsilon': 1.0, 'sigma': 1.0},
                                           '1-2': {'epsilon': 1.5, 'sigma': 0.8},
                                           '2-2': {'epsilon': 0.5, 'sigma': 0.88}},
                            'type': 'lennard_jones'}],
             'reference': 'W. Kob and H. C. Andersen, Phys. Rev. Lett. 73, 1376 (1994)',
             }
        self.assertEqual(m, db.schema._convert(m, 1))
        self.assertEqual(n, db.schema._convert(m, 2))

    def test_schemas(self):
        from jsonschema import validate
        for v in [1, 2]:
            for model in db.models.search(db.query.schema_version == v):
                validate(instance=model, schema=db.schemas[v])
        
    def test_tinydb_compat(self):
        from tinydb import TinyDB, Query
        from tinydb.storages import MemoryStorage
        self.skipTest('')
        
        db = TinyDB(storage=MemoryStorage)
        db.default_table_name = 1
        for version in [1, 2]:
            table = db.table(version)
            table.insert_multiple(db.models(version))
        self.assertTrue(len(db.table(1)) > 0)
        self.assertTrue(len(db.table(2)) > 0)

    def test_paths(self):
        for entry in db.samples:
            entry['path']
        
    def test_storage(self):
        samples = db.samples.copy((db.query.model == 'lennard_jones') &
                                  (db.query.density == 1.0))[0]
        self.assertTrue(len(samples) > 0)

    def test_grammar(self):
        db.models
        db.model("lennard_jones")
        db.samples
        db.sample("storage/coslovich_pastore/0_488db481cdac35e599922a26129c3e35.xyz")
        local = db.samples.copy(db.query.model == 'lennard_jones')

    def test_pprint(self):
        samples = db.samples.search((db.query.model == 'lennard_jones') &
                                    (db.query.density == 1.0))
        # db.pprint(samples)

    def test_missing(self):
        import os
        db.database.progress = lambda x: x
        #rmf("/tmp/db.json")
        _db = db.Database("/tmp/db.json")
        _db.add_hook(db.hooks.metadata_from_path)
        _db.add_hook(db.hooks.metadata_from_atooms)
        _db.insert_glob("/tmp/dataset/**/config.xyz", copy=True)
        rmf(_db.all()[0]['absolute_path'])
        self.assertEqual(len(_db._missing()), 1)
        _db.update()
        self.assertEqual(len(_db._missing()), 0)

    def test_search_with_hooks(self):
        import os
        db.database.progress = lambda x: x
        for dbfile in ['', "/tmp/db.json"]:
            _db = db.Database(dbfile)
            _db.add_hook(db.hooks.metadata_from_path)
            _db.add_hook(db.hooks.metadata_from_atooms)
            _db.insert_glob("/tmp/dataset/**/config.xyz", copy=False)
            samples = _db.search(db.query.n == 1)
            self.assertTrue(len(samples) > 0)
        
    def test_insert_no_requires(self):
        db = _TinyDB()
        db.insert({'foo': 'bar'})
        
    def test_pprint_db(self):
        db = _TinyDB()
        db.insert({'foo': 1, 'bar': 3})
        db.insert({'foo': 2, 'bar': 2})
        db.insert({'foo': 3, 'bar': 1})
        # db.pprint()
        self.assertEqual(db.rows(sort_by='bar')['foo'], [3, 2, 1])
        
    def tearDown(self):
        rmd('/tmp/dataset')
        rmf("/tmp/db.json")


if __name__ == '__main__':
    unittest.main()
