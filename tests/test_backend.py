import os
import unittest
import atooms.trajectory
import atooms.database
try:
    import rumd
    import atooms.database.rumd.interaction
    from atooms.backends.rumd import RUMD
    SKIP = False
except ImportError:
    SKIP = True

class Test(unittest.TestCase):

    def setUp(self):
        if SKIP:
            self.skipTest('missing RUMD')
        self.root = os.path.dirname(__file__)

    def test_(self):
        atooms.database.default_schema_version = 2
        for model in ['kob_andersen', 'coslovich_pastore']:
            pot = atooms.database.rumd.interaction.potential(model)
        atooms.database.default_schema_version = 1

    def test_cp(self):
        inp = os.path.join(self.root, '../atooms/database/samples/coslovich_pastore_N300_rho1.655_T0.31.xyz')
        atooms.trajectory.TrajectoryXYZ(inp).copy(cls=atooms.trajectory.TrajectoryRUMD, fout='/tmp/1.xyz')
        model = 'coslovich_pastore'
        potentials = atooms.database.rumd.interaction.potential(model)
        backend = RUMD('/tmp/1.xyz', potentials=potentials)
        epot_0 = backend.system.potential_energy(per_particle=True)

        model = 'coslovich_pastore'
        model = atooms.database.get(model)
        model['cutoff'][0]['type'] = 'linear_cut_shift'
        model['cutoff'][0]['parameters']['rcut'] = model['cutoff'][0]['parameters']['rspl']
        model['cutoff'][0]['parameters'].pop('rspl')
        with atooms.trajectory.Trajectory(inp) as th:
            s = th[0]
            s.species_layout = 'F'
            s.interaction = atooms.database.f90.interaction.Interaction(model)
            epot_1 = s.potential_energy(per_particle=True)
        self.assertLess(abs(epot_0 - epot_1), 1e-6)
        
    def test_ka(self):
        inp = os.path.join(self.root, '../atooms/database/samples/kob_andersen_N150_rho1.2.xyz')
        atooms.trajectory.TrajectoryXYZ(inp).copy(cls=atooms.trajectory.TrajectoryRUMD, fout='/tmp/1.xyz')
        model = 'kob_andersen'
        potentials = atooms.database.rumd.interaction.potential(model)
        backend = RUMD('/tmp/1.xyz', potentials=potentials)
        epot_0 = backend.system.potential_energy(per_particle=True)

        model = 'kob_andersen'
        with atooms.trajectory.Trajectory(inp) as th:
            s = th[0]
            s.species_layout = 'F'
            s.interaction = atooms.database.f90.interaction.Interaction(model)
            epot_1 = s.potential_energy(per_particle=True)
        self.assertLess(abs(epot_0 - epot_1), 1e-6)

    def tearDown(self):
        import os
        os.system('rm -f /tmp/1.xyz')


if __name__ == '__main__':
    unittest.main()
