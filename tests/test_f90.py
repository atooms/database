import os
import unittest
import atooms.database
from atooms.trajectory import Trajectory
from atooms.system import System, Particle, Cell
from atooms.backends.f90 import Interaction, NeighborList, VerletList


class Test(unittest.TestCase):

    def setUp(self):
        self.fileinp = os.path.join(os.path.dirname(__file__), '../atooms/database/storage/lennard_jones/0_13ce47602b259f7802e89e23ffd57f19.xyz')
        self.trajectory = Trajectory(self.fileinp)

    def test_collinear(self):
        model = atooms.database.model("lennard_jones")
        particles = [Particle(position=[0.0, 0.0, 0.0], species=1),
                     Particle(position=[1.0, 0.0, 0.0], species=1),
                     Particle(position=[2.0, 0.0, 0.0], species=1)]
        cell = Cell([10., 10., 10.])
        system = System(particles, cell)
        system.interaction = Interaction(model)
        self.assertAlmostEqual(system.potential_energy(), -0.01257276409199999)

    def test_interface(self):
        from atooms.database.f90 import Interaction
        particles = [Particle(position=[0.0, 0.0, 0.0], species=1),
                     Particle(position=[1.0, 0.0, 0.0], species=1),
                     Particle(position=[2.0, 0.0, 0.0], species=1)]
        cell = Cell([10., 10., 10.])
        system = System(particles, cell)
        system.interaction = Interaction("lennard_jones")
        self.assertAlmostEqual(system.potential_energy(), -0.01257276409199999)

    def test_ntw(self):
        trajectory = Trajectory(os.path.join(os.path.dirname(__file__), '../atooms/database/storage/coslovich_pastore/0_488db481cdac35e599922a26129c3e35.xyz'))
        system = trajectory[0]
        system.species_layout = 'F'
        model = atooms.database.model('coslovich_pastore')
        system.interaction = Interaction(model)
        self.assertAlmostEqual(system.potential_energy(per_particle=True), -6.0295, places=4)
        trajectory.close()
        
    def test_ntw_vl(self):
        trajectory = Trajectory(os.path.join(os.path.dirname(__file__), '../atooms/database/storage/coslovich_pastore/0_488db481cdac35e599922a26129c3e35.xyz'))
        system = trajectory[0]
        system.species_layout = 'F'
        model = atooms.database.model('coslovich_pastore')
        system.interaction = Interaction(model)
        system.interaction.neighbor_list = VerletList()
        self.assertAlmostEqual(system.potential_energy(per_particle=True), -6.0295, places=4)
        trajectory.close()
       
    def test_poly(self):
        # TODO: temporary path
        self.skipTest('fix me')
        trajectory = Trajectory('data/config.h5.min.xyz')
        model = {
            "cutoff": [{
                "type": "cut", "parameters": {"rcut": [[1.25]]}
            }],
            "potential": [{
                "type": "sum_inverse_power",
                "parameters": {"sigma": [[[1.0, 1.0, 1.0, 1.0]]],
                               "epsilon": [[[1.0, -0.990791918022, 1.141392289561, -0.332041393327]]],
                               "exponent": [18, 0, -2, -4]}
            }],
        }
        system = trajectory[0]
        system.species_layout = 'F'
        system.interaction = Interaction(model)
        self.assertAlmostEqual(system.potential_energy(per_particle=True), 1.532014403624, places=10)
        self.assertAlmostEqual(system.interaction.forces[0, 0], -0.00016927648667763862)
        trajectory.close()

    def tearDown(self):
        self.trajectory.close()


if __name__ == '__main__':
    unittest.main()
